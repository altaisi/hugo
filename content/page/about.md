---
title: About Us
subtitle: Istanbul Dental Tours provides high level of dental service in accordance with international standards and at a very reasonable price. We attract our patients from all around the world, especially from Europe, U.S.A. and the Middle East. We provide them an accurate and reliable dental services where we focus on patient’s comfort and wishes.

comments: false
---

We are located in Istanbul and working with top 3 clinics in Istanbul. We are highly energetic and well-organized group of enthusiasts dedicated to perfect customer experience. We are dental services partner of USA Embassy in Istanbul.

We can organize your flight, transfer, accommodation and Istanbul tour as well as your operations in clinic and we have proudly served more than 1752 patients from all over the world.

### Let's Talk

You can [book a free online video consultation](https://www.istanbuldentaltours.com/) and our dentists will answer all of your questions and offer a treatment.
